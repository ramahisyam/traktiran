package com.traktiran.traktiran

class User {
    var id: String? = null
    var firstName: String? = null
    var lastName: String? = null
    var email: String? = null
    var password: String? = null

    constructor(){}

    constructor(firstName: String?, lastName: String?, email: String?, password: String?) {
        this.firstName = firstName
        this.lastName = lastName
        this.email = email
        this.password = password
    }

    constructor(id: String?, firstName: String?, lastName: String?) {
        this.id = id
        this.firstName = firstName
        this.lastName = lastName
    }


    fun toMap(): Map<String, Any> {
        val result = HashMap<String, Any>()
        result.put("firsname", firstName!!)
        result.put("lastname", lastName!!)

        return result
    }

}