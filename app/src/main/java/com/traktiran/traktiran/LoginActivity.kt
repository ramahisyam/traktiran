package com.traktiran.traktiran

import android.app.ProgressDialog
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.TextUtils
import android.view.View
import com.google.firebase.auth.FirebaseAuth
import com.wajahatkarim3.easyvalidation.core.view_ktx.validEmail
import com.wajahatkarim3.easyvalidation.core.view_ktx.validator
import kotlinx.android.synthetic.main.activity_login.*
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast

class LoginActivity : AppCompatActivity() {

    private val TAG = "LoginActivity"

    private var email: String? = null
    private var password: String? = null

    private var progressDialog: ProgressDialog? = null

    private var mAuth: FirebaseAuth? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        progressDialog = ProgressDialog(this)

        mAuth = FirebaseAuth.getInstance()
    }

    fun signUp(view: View) {
        startActivity<RegistrationActivity>()
    }

    fun signIn(view: View) {
        loginUser()
    }

    private fun loginUser() {

        email = edt_email_in.text.toString()
        password = edt_password_in.text.toString()

        if (!TextUtils.isEmpty(email) && !TextUtils.isEmpty(password)) {
            progressDialog!!.setMessage("Wait a second...")
            progressDialog!!.show()

            mAuth!!.signInWithEmailAndPassword(email!!, password!!)
                .addOnCompleteListener (this){ task ->

                    progressDialog!!.hide()

                    if (task.isSuccessful) {
                        startActivity<MainActivity>()
                    } else {
                        toast("auth failed " + task.exception)
                        progressDialog!!.hide()
                    }

                    val user = mAuth!!.currentUser!!.uid
                    if (user != null) {
                        finish()
                    }
                }
        } else {
            edt_email_in.validEmail {
                edt_email_in.error = "Email cannot be empty"
            }
            edt_password_in.validator()
                .nonEmpty()
                .addErrorCallback {
                    edt_password_in.error = "Password cannot be empty"
                }
                .check()
        }

    }
}
