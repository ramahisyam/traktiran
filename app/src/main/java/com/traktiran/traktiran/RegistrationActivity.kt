package com.traktiran.traktiran

import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.TextUtils
import android.util.Log
import android.view.View
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.wajahatkarim3.easyvalidation.core.view_ktx.nonEmpty
import com.wajahatkarim3.easyvalidation.core.view_ktx.validEmail
import com.wajahatkarim3.easyvalidation.core.view_ktx.validator
import kotlinx.android.synthetic.main.activity_registration.*
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast

class RegistrationActivity : AppCompatActivity() {

    private val TAG = "RegistrationActivity"

    private lateinit var firstName: String
    private lateinit var lastName: String
    private lateinit var email: String
    private lateinit var password: String

    private var mProgressBar: ProgressDialog? = null

    private var firestoreDB: FirebaseFirestore? = null
    private var mAuth: FirebaseAuth? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registration)

        mProgressBar = ProgressDialog(this)
        firestoreDB = FirebaseFirestore.getInstance()
        mAuth = FirebaseAuth.getInstance()
    }

    fun signIn(view: View){
        val intent = Intent(this, LoginActivity::class.java)
        startActivity(intent)
    }

    fun signUp(view: View) {
        firstName = edt_firstname.text.toString()
        lastName = edt_lastname.text.toString()
        email = edt_email_up.text.toString()
        password = edt_password_up.text.toString()

        if (!TextUtils.isEmpty(firstName) && !TextUtils.isEmpty(lastName)
            && !TextUtils.isEmpty(email) && !TextUtils.isEmpty(password)) {

            mProgressBar!!.setMessage("Registering User...")
            mProgressBar!!.show()

            mAuth!!
                .createUserWithEmailAndPassword(email!!, password!!)
                .addOnCompleteListener(this) { task ->
                    if (task.isSuccessful){
                        mProgressBar!!.hide()

                        val userId = mAuth!!.currentUser!!.uid

                        verifyEmail()
                        addUser(userId, firstName, lastName)
                        startActivity<MainActivity>()
                        if (userId != null) {
                            finish()
                        }
                    } else {
                        Log.d(TAG, "Failure with : ", task.exception)
                        edt_password_up.error = "Password must be 6 character"
                        mProgressBar!!.hide()
                    }
                }
        } else {
            edt_firstname.nonEmpty {
                edt_firstname.error = "First Name cannot be empty"
            }
            edt_lastname.nonEmpty {
                edt_lastname.error = "Last Name cannot be empty"
            }
            edt_email_up.validEmail {
                edt_email_up.error = "Email cannot be empty"
            }
            edt_password_up.validator()
                .nonEmpty()
                .addErrorCallback { edt_password_up.error = "Password cannot be empty" }
                .check()
        }
    }

    private fun verifyEmail() {
        val mUser = mAuth!!.currentUser
        mUser!!.sendEmailVerification()
            .addOnCompleteListener(this) {task ->
                toast("Verification sent to " + mUser.email)
            }
    }

    private fun addUser(id: String, firstName: String, lastName: String){
        val user = User(id, firstName, lastName).toMap()

        firestoreDB!!.collection("users")
            .document(id)
            .set(user)
            .addOnCompleteListener {
                toast("Success")
            }
            .addOnFailureListener {
                toast("Failed")
            }
    }
}
